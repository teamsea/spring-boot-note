# SpringBoot应用加强 #

## 1  SpringBoot 健康检查	 ##

SpringBoot可以实现起步依赖,和自动加载Bean对象

我们可以使用SpringBoot健康检查来实现对Bean对象的健康

这个功能一般会在开发和测试中使用,在上线后建议删除

添加健康检查的依赖

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

 配置后启动服务器,可以访问[http://localhost/actuator/beans](http://localhost/actuator/beans)来进行健康检查

在`Boot Dashboard`中的`propertise`界面上查看

## 2 SpringBoot 热加载 ##

对于普通的spring项目和SpringBoot项目 ,在改动Java代码时,我们是需要重新编译,启动服务器的

热部署实现在不关停服务器的情况下,自动编译和加载Java类和文件

添加热部署的依赖:

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-devtools</artifactId>
	<scope>runtime</scope>
</dependency>
```

说明:当我们修改了`src/main/java`目录下的java文件或修改了`src/main/resources`目录下的配置文件时，默认都会重启你的web服务器，但是修改了测试类或html文件不会自动重启和部署。

## 3 Lombok 插件 ##

Lombok是第三方的插件,它并不属于SpringBoot

Lombok提供了一些API可以再编译过程中提供一些方法,比如:`Setter`和`Getter`方法等

#### 添加LomBok依赖 ####

```xml
<!--lombok 插件依赖-->
<dependency>
	<groupId>org.projectlombok</groupId>
	<artifactId>lombok</artifactId>
</dependency>
```

#### 安装Lombok插件 ####

一种万能的方法来添加LomBok插件,是Lombok对编辑器进行的通用安装

1. 寻找LomBok插件的jar包

   一般在Maven仓库中

   ![1694761541668-2023-9-1515:05:42.png](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1694761541668-2023-9-1515:05:42.png)

   ![image-20230915150618727](C:\Users\lknlk\AppData\Roaming\Typora\typora-user-images\image-20230915150618727.png)

2. 在系统资源管理器中打开这个文件夹

   ![1694761694973-2023-9-1515:08:15.png](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1694761694973-2023-9-1515:08:15.png)

3. 使用cmd`java -jar`命令来实现打开这个jar包

   ![1694761823143-2023-9-1515:10:23.png](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1694761823143-2023-9-1515:10:23.png)

   ![1694761846456-2023-9-1515:10:47.png](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1694761846456-2023-9-1515:10:47.png)

4. 选择我们的编辑器(IDEA)进行安装(Install)

   安装成功后,编辑器(IDEA)的根目录会多出一个`lombok.jar`

   ![1694762037188-2023-9-1515:13:57.png](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1694762037188-2023-9-1515:13:57.png)

   同时在`.ini`程序初始文件中会有添加一句话

   ![1694762120720-2023-9-1515:15:21.png](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1694762120720-2023-9-1515:15:21.png)

#### 使用Lombok插件 ####

Lombok插件安装完毕后,重启编辑器(IDEA),让编辑器加载LomBok插件

LomBok插件一般是针对`pojo`实体类进行操作的

| 注解                | 含义                                                         |
| ------------------- | ------------------------------------------------------------ |
| @Setter             | 用于为描述的类生成setter方法,不包含final修饰属性             |
| @Getter             | 用于为描述的类生成getter方法                                 |
| @ToString           | 用于为描述的类添加toString方法                               |
| @EqualsAndHashCode  | 用于为描述的类，生成hashCode和equals方法                     |
| @NoArgsConstructor  | 用于为描述的类生成无参的构造方法                             |
| @AllArgsConstructor | 用于为描述的类生成包含类中所有字段的构造方法                 |
| @Data               | 用于为描述的类生成setter/getter、equals、canEqual、hashCode、toString方法，如为final属性，则不会为该属性生成setter方法 |
| @Slf4J              | 用于为描述的类添加一个日志属性对象                           |

实例:

```java
/**
 * 活动类 使用LomBok插件进行优化
 * 
 * 使用注解@Data生成Getter and Setter ToString
 * 
 * 使用注解@NoArgsConstructor生成无参构造
 * 
 * 使用注解@AllArgsConstructor生成全参构造
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Activity {

	private Integer id; // id属性
	private String title; // 标题
	private String category; // 等级

	// 因为Date 是一个过期的类型(有各种个样的问题)
	// 现在使用的日期类型是 JDK8 中出现的 LocalDateTime 工具类
	// 使用Spring框架提供的DateTimeFormat注解对时间类型格式进行规范
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	private LocalDateTime startTime; // 开始时间
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	private LocalDateTime endTime; // 结束时间
	
	private Byte state; // 状态
	private String remark;// 内容正文
	private String createUser; // 创建用户
	
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	private LocalDateTime createdTime; // 创建时间

}
```

测试类

```java
@Test
public void testLombok() {
	
	Activity activity = new Activity();
	
	Class clazz =  activity.getClass();
		
	Method[] methods = clazz.getMethods();
		
	for (Method method : methods) {
		System.out.println(method.getName());
	}
}
```

输入结果:

```
equals
toString
hashCode
getId
getState
setState
getCreatedTime
setId
setTitle
setCategory
getRemark
setStartTime
getTitle
setEndTime
setRemark
setCreateUser
setCreatedTime
getEndTime
getCategory
getStartTime
getCreateUser
wait
wait
wait
getClass
notify
notifyAll
```

在测试类中使用`Slf4J`注解

```java
@SpringBootTest
@Slf4j
public class ServiceUpdateSQLTest {

	/**
	 * 使用Logger org.slf4j.Logger
	 * 
	 * 在使用logger对象时,要创建成一个静态常量
	 * 
	 * 1. 稳定的运行日志框架
	 * 
	 * 2. 在类加载前就是用这个框架
	 */
	//使用Lombok插件进行优化
	//	private static final Logger logger = LoggerFactory.getLogger(ActivityServiceImpl.class)

	@Autowired
	private ActivityService service;

	/**
	 * 
	 * 获取活动列表
	 */
	@Test
	public void getActivitiesListTest() {
		// 使用Logger
		log.info("start: {}", System.currentTimeMillis());
		log.info(service.getActivities().toString());
		log.info("end: {}", System.currentTimeMillis());
	}
}
```

