# AJAX技术 #

<!-- 
	关于AJAX异步技术,现在已经出现了原生ajax,JQ简化ajax,axois还有fetch技术
	这里会对每一种技术进行讲解
-->

# 1 Ajax技术 #

## 1.1Ajax 是什么？   ##

Ajax (Asynchronous JavaScript and XML) 是一种Web应用技术,可以借助客户端脚本(javascript)与服务端应用进行异步通讯，获取服务端数据以后,可以进行局部刷新。进而提高数据的响应和渲染速度。

传统的请求访问模式

<img src="https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/image8-2023-9-1515:50:56.png" alt="image8-2023-9-1515:50:56.png" style="zoom:67%;" />

异步请求模式

<img src="https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/image1-2023-9-1515:51:26.png" alt="image1-2023-9-1515:51:26.png" style="zoom: 67%;" />

## 1.2Ajax 技术应用场景？ ##

Ajax技术最大的优势就是底层异步,然后局部刷新,进而提高用户体验,这种技术现在在很多项目中都有很好的应用,例如:

- 商品系统。
- 评价系统。
- 地图系统。

​    AJAX可以仅向服务器发送并取回必须的数据，并在客户端采用JavaScript处理来自服务器的响应。这样在服务器和浏览器之间交换的数据大量减少，服务器响应的速度就更快了。但Ajax技术也有劣势，最大劣势是不能直接进行跨域访问。

## 1.3Ajax 技术时序模型分析 ##

传统Web应用中的，同步请求应用时序模型分析

客户端向服务端向服务端发送请求需要等待服务端的响应结果，服务端返回数据以后，客户端可以继续发送请求。

<img src="https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/image9-2023-9-1515:54:10.png" alt="image9-2023-9-1515:54:10.png" style="zoom:67%;" />

基于Ajax技术的Web异步请求响应模型如图

客户端可以向服务端发送异步请求，客户端无需等待服务端的响应结果，可以不断向服务端发送请求。

<img src="https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/image5-2023-9-1515:54:40.png" alt="image5-2023-9-1515:54:40.png" style="zoom:67%;" />

# 2 原生Ajax #

## 2.1 Ajax 请求响应过程分析 ##

所有的Ajax 请求都会基于DOM(HTML元素)事件，通过XHR（XMLHttpRequest）对象实现与服务端异步通讯局部更新

<img src="https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/image7-2023-9-1515:56:07.png" alt="image7-2023-9-1515:56:07.png" style="zoom:67%;" />

## 2.2 Ajax 编程基本步骤分析 ##

第一步：基于dom事件创建XHR对象(XMLHttpRequest对象)

第二步：注册XHR对象状态监听，通过回调函数处理状态信息。

第三步：创建与服务端的连接

第四步：发送异步请求实现与服务端的通讯

第五步：通过回调（callback）函数，获得响应结果并进行数据更新.(非阻塞)

```javascript
    // 声明XMLHttpRequest对象
    var httpRequest = new XMLHttpRequest();

	// 对事件绑定回调函数
	httpRequest.onreadystatechange = function () {}

    // 打开连接	设置请求参数,访问地址,是否异步
    httpRequest.open("Method", "URL", true);

    // 实现连接
    httpRequest.send();

	// 注意AJAX有跨域的问题,在浏览器访问时使用和URL一样的地址来访问这个域名进行使用,不要使用随意使用
```

## 2.3 使用原生Ajax来实现异步请求 ##

```javascript
// 获取xhr对象
var xhr = new XMLHttpRequest();

// 对xhr对象事件绑定一个回调函数
xhr.onreadystatechange = function () {
        console.log(httpRequest.statusText);
}

// 打开连接
xhr.open("GET", "http://127.0.0.1:8080/activity/dofindactivities", true);

// 实现连接
httpRequest.send();
```

## 2.4 使用Json数据条来传递数据 ##

后端向前端传递数据,可以使用Json数据条来传递

json可以被原生的JavaScrip解析,同时解析速度更快

在Controller方法上加上`@ResponseBody`注解进行描述,SpringBoot会将这个方法的返还值转换为Json数据条

```java
@RequestMapping("dofindactivities")
@ResponseBody
public List<Activity> doFindActivities() {
	List<Activity> list = service.getActivities();
	return list;
}
```

Json数据条可以放置到前端浏览器进行解析

### 2.4.1 解析JSON数据条 ###

Json数据本质是一个数组(被中括号`[]`包裹),这个数组中含有被`{}`包裹的对象

可以使用`for`循环对这个数组进行输出

```javascript
// 在回调函数中处理json
httpRequest.onreadystatechange = function () {
	var activities = JSON.parse(httpRequest.responseText);
    // 打印这个数据条
    console.log(activities); 
    // 获取数据条中的每一个对象数据
    for (var i = 0; i < activities.length; i++) {
        console.log(activities[i]);
    }
}
```

# 3 JQ简化Ajax #

JQuery对Js实现了封装,也提供了对Ajax的操作

使用函数`ajax(url,[setting])`

+ `url`	ajax要请求的地址
+ `[setting]` 	ajax的请求参数

| 属性名称 | 值类型                     | 含义                                                         |
| -------- | -------------------------- | ------------------------------------------------------------ |
| url      | String                     | 请求地址                                                     |
| type     | `GET`,`POST`               | 请求方式                                                     |
| data     | Object                     | 请求提供的参数                                               |
| dataType | `json`,`xml`,`html`,`text` | 请求返回结果的值类型                                         |
| success  | Function                   | 请求成功执行的回调函数,请求的结果会放置到回调函数的参数列表中 |
| error    | Function                   | 请求失败执行的回调函数                                       |

```javascript
// 异步请求 实现登录操作
function login() {
        var name = $('#name').val();
        var password = $('#password').val();
        //TODO:使用AJAX将数据发送到服务器
        $.ajax({
            url: 'login', // 请求地址
            type: 'GET', // 请求方法
            data: {
                'name': name,
                'password': password
            },

            dataType: "json",

            success: function (data) {
                getData(data);
            },

            error: function (error) {
                console.log(error);

            }
        });
    }
```

<!--
	现阶段先更新到使用JQuery来简化对Ajax的操作
-->

# 4 axois Node.js #

# 5 fetch 实现Ajax #
