# Spring Boot入门

## Spring Boot 简介

### Spring Boot 概述

现在软件市场已经形成一定的规模,系统架构的复杂度越来越高

程序的开发更加注重于开箱即用更注重于技术在生态圈中的深度融合,更注重于轻量级的运维

### Spring Boot 核心特性

Spring Boot 其实是一个脚手架,Spring Boot构建在Spring框架之上

Spring Boot 提供了自动配制功能,可以实现开箱即用

其中的核心特点:

+  起步依赖 
+ 自动配置
+ 健康检查

Spring Boot 官方网址: https://spring.io/projects/spring-boot

### 下载安装Spring Tools 工具

这个工具实对Eclipse 等开发工具进行插件升级,更好的对SpringBoot 项目创建和管理

下载地址[Spring | Tools](https://spring.io/tools/)

下载完成后一般是一个`.jar`文件使用`java - jar`命令来进行解压和安装

这个工具和Eclipse工具是一样的,更改Maven配置和创建项目于Eclipse一致

## Spring Boot 快速入门

创建一个Spring Boot项目

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/16770782859071677078285752.png)



更改项目设置

<img src="https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/16770802879071677080287410.png" style="zoom: 80%;" />

添加依赖,现在的Spring Boot 版本实3.0.2

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/16770803659091677080365368.png)

最后点击Finish来完成创建项目

在创建项目之后STS会下载和配置相关环境,可以讲Maven的镜像仓库更换为国内的

## 项目结构

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/16770828869081677082886631.png)

其中包括

`SpringstartApplication.java` 	Spring Boot 入口文件

`application.properties`	Spring Boot 配置文件

`SpringstartApplicationTests.java`Spring Boot 测试类

## Spring Boot 框架原理

### Spring Boot 框架启动原理

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1677152390851image4.png)

1. 通过被`@SpringBootApplication`注解修饰的启动类来获取到整个项目的所有包和class 文件
2. 讲所有的class 文件加载到内存中,成为一个个Class对象
3. 获取Class对象的接口来分类管理和创建对应额对象和工厂类
4. 创建类的实例将实例化对象加入到Bean管理池中

根据Spring Boot 启动原理可得:

+ 如果想将bean组件交给Spring Boot框架进行管理,则这个Bean组件需要在启动类同包或者是其子包中
+ SpringBoot 框架管理的测试类,需要被`@SpringBootTest`注解修饰
+ 由于Spring Boot 创建对象的方式,所有的Bean组件应该要提供属性的Getter/Setter方法,还有一个无参构造方法

## Spring Boot Bean管理和应用

![测试类运行后的bean组件注入过程](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1677156320856image19.png)

Spring Bean 组件管理是Spring 框架中的基本操作

在Spring 框架中,需要在配置文件中进行加入(当然可以使用注解)

### 声明和使用Bean组件

Spring Boot框架使用`@Component`注解来声明Bean组件

```java
@Component
public class User {
	private String name;
	private String id;
}
```

可以使用`@Autowired`注解进出依赖注入

```java
@Autowired
private User user;	
```

### Bean 组件及其声明周期管理

![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1677157849856image17.png)

spring 框架管理bean组件默认使用的单例模式

Spring Boot 管理bean组件也是使用的是单例模式,根据Spring Boot 启动原理可知,Spring Boot 创建Bean使用的是饿汉式

1. 懒汉式加载

   使用`@Lazy`注解,对bean组件进行懒汉式加载

2. 单例模式,多例模式

   使用`@Scope`注解,其中`singleton`属性为单例模式,`prototype`属性为多例模式

   如果一个Bean组件被`@Scope("prototype")`修饰,则SpringBoot 框架只会创建这个Bean组件的实例,创建后Spring Boot框架不会在对这个对象进行管理了

被Spring Boot 创建的Bean 组件会进行创建还有销毁

在创建时要执行的方法需要被`@PostConstruct`注解修饰

在销毁时要执行的方法需要被`@PreDestroy`注解修饰

```java
/**
 * 这个用户测试Spring Boot注入依赖,生命周期,Bean组件管理
 * 	
 * 创建两个工具Bean种子类,两个都是单例模式
 * 其中一个进行懒加载,一个直接进行加载
 * 
 * 创建一个User类和一个File类
 * 其中User修饰为多例模式,File设计为单例模式
 * 
 * 这四个类同事声明声明周期方法
 *  
 *  */
@SpringBootTest
public class SpringBootBeanTest {
	
	@Autowired
	private User user;
	
//  这里注释掉依赖使用的代码,不使用UserToolBean这个Bean种子   
//	@Autowired 
//	private UserToolBean userToolBean; 
	
	@Test
	public void beanTest() {
		
		System.out.println(user);
		
	}
	
	@Test
	public void userToolsTest() {
//		System.out.println(userToolBean);
	}
	
}
```

四个Bean种子类

```java
@Component
public class File {
    
	@PostConstruct
	private void init() {
		System.out.println("File	已经创建");
	}
	
	@PreDestroy
	private void destroy() {
		System.out.println("File	已经销毁");
	}
}
```

```java
@Component
@Scope("prototype")
public class User {
	@PostConstruct
	private void init() {
		System.out.println("User	已经创建");
	}

	@PreDestroy
	private void destroy() {
		System.out.println("User	已经销毁");
	}
}
```

```java
/**
 * 	文件管理类用于对文件的一些操作和管理
 * 	
 * 	单例
 * */

@Component
@Scope("singleton")
public class FileToolsBean {
	@PostConstruct
	private void init() {
		System.out.println("FileTool创建");
	}
	
	@PreDestroy
	private void destroy() {
		System.out.println("FileTool销毁");
	}
	
}

```

```java
/**
 * 这个工具类使用来对User进行管理和操作的
 * 设定为单例,但是要懒加载
 * 
 * */

@Component
@Scope("singleton")
@Lazy
public class UserToolBean {

	@PostConstruct
	private void init() {
		System.out.println("UserTools创建");
	}
	
	@PreDestroy
	private void destroy() {
		System.out.println("UserTools销毁");
	}
	
}
```

第一个测方法的测试结果

```
File	已经创建
FileTool创建
User	已经创建
com.teamsea.bean.User@70d63e05
FileTool销毁
File	已经销毁
```

结果分析

+ 由于UserToolBean种子被`@Lazy`注解修饰所以不使用UserToolBean不会加载这个Bean种子

  而没有被注解修饰的Bean种子都被加载了

+ User Bean组织被`@Scope("prototype")`修饰,所以这个Bean组件只会被Spring Boot创建但是不会被Spring销毁

+ 常用的开发策略声明周期使用方法

  懒加载:对象暂时用不到,所以可以不用提前加载

  单例模式:当一个对象频繁使用时可使用单例模式,但是要考虑的是这个对象在存储中只有一份

  生命周期方法:更好的对实例化对象进行初始化和销毁

+ <font color="red">Spring Boot 框架管理Bean组件的优势</font>

  Spring 框架可以基于用户设计管理对象与对象依赖关系,以降低对象与对象之间的直接耦合,提高程序之间的可扩展型和可维护性

  Spring 是一个资源整合框架,通过Spring可将很多资源整合在一起,进行科学的应用,以便更好的对外提供服务

  低耗,高效,解耦合

### Bean组件冲突问题

当一个接口有多个实现类,并且都是通过Spring管理的Bean组件

当在给这个接口注入依赖时就会出现报错

```java
interface Cache{}
```

```java
@Component
public class DefaultCache implements Cache{}
@Component
public class WeakCache implements Cache{}
```

```java
@SpringBootTest
public class SpringBootTest{
    @Autowired
    private Cache cache;
    
    @Test
    public void test(){
        System.out.println("cache");
    }
}
```

运行结果:

```
org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'com.teamsea.beanteast.BeanTestPuls': Unsatisfied dependency expressed through field 'caChe'; nested exception is org.springframework.beans.factory.NoUniqueBeanDefinitionException: No qualifying bean of type 'com.teamsea.cache.CaChe' available: expected single matching bean but found 2: defaultCaChe,standbyCaChe
```

结果分析:

有两个`CaChe`接口的实现类,都可以成为Bean种子所以会产生依赖冲突问题

解决方法:

+ 先将两个实现类上的`@Component`注解加入属性进行命名,命名不能相同
+ 使用`@Qualifier`注解对注入的依赖进行确认要注入哪一个依赖

```java
@Component("defaultCache")
public class DefaultCache implements Cache{}
@Component("weakCache")
public class WeakCache implements Cache{}
```

```java
@SpringBootTest
public class SpringBootTest{
    @Autowired
    @Qualifier("defaultCache")
    private Cache cache;
    
    @Test
    public void test(){
        System.out.println("cache");
    }
}
```

注意实现类命名不能相同,会包异常:bean同名异常

 ![](https://gitee.com/teamsea/tuchuang/raw/master/tuchuang/1677579661168image2.png)

1. @Autowired注解应用规则?
2. @Qualifier注解的作用是什么?

 @Autowired由spring框架定义，用于描述类中属性或相关方法。Spring框架在项目运行时假如发现由他管理的Bean对象中有使用@Autowired注解描述的属性或方法，可以按照指定规则为属性赋值(DI)。其基本规则是：首先要检测容器中是否有与属性或方法参数类型相匹配的对象，假如有并且只有一个则直接注入。其次，假如检测到有多个，还会按照@Autowired描述的属性或方法参数名查找是否有名字匹配的对象，有则直接注入，没有则抛出异常。最后，假如我们有明确要求，必须要注入类型为指定类型，名字为指定名字的对象还可以使用@Qualifier注解对其属性或参数进行描述(此注解必须配合@Autowired注解使用)。

